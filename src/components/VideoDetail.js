import React from 'react';

const VideoDetail = ({ selectedVideo }) => {
    if (!selectedVideo) {
        return (
            <p>Loading...</p>
        );
    }

    let videoSrc = `https://www.youtube.com/embed/${selectedVideo.id.videoId}`;

    return ( 
        <React.Fragment>
            <div className="ui embed">
                <iframe src={videoSrc} title={selectedVideo.snippet.description}/>
            </div>
            <div className="ui segment">
                <h4 className="ui header">{selectedVideo.snippet.title}</h4>
                <p>{selectedVideo.snippet.description}</p>
            </div>
        </React.Fragment>
    );
}
 
export default VideoDetail;
