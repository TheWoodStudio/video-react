import React from 'react';

const VideoItem = ({ video, onVideoSelect }) => {
    return ( 
        <div className="item" onClick={() => onVideoSelect(video)}>
            <div className="image-container">
                <img className="ui image" src={video.snippet.thumbnails.medium.url} alt={video.snippet.title}/>
            </div>
            <div className="content">
                <h6 className="header" href="/">{video.snippet.title}</h6>
            </div>
        </div>
     );
}
 
export default VideoItem;
