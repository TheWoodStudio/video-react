import React, { Component } from 'react';

class SearchBar extends Component {
    state = { 
        inputField: ''
    }

    onInputChange = (e) => {
        this.setState({
            inputField: e.target.value
        });
    }

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.searchSubmit(this.state.inputField);
    }

    render() { 
        return ( 
            <form className="ui form" onSubmit={this.onFormSubmit}>
                <div className="search-bar ui fluid icon input">
                    <input type="text" placeholder="Search video" onChange={this.onInputChange} value={this.state.inputField}/>
                    <i className="search icon"></i>
                </div>
            </form>
         );
    }
}
 
export default SearchBar;