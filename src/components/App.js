import React, { Component } from 'react';
import youtube from '../api/youtube';
import Header from './Header';
import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

class App extends Component {
  state = {
    videos: [],
    selectedVideo: null
  }

  componentDidMount() {
    this.searchSubmit();
  }

  searchSubmit = async (search = 'ducklings') => {
    const response = await youtube.get('/search', {
      params: {
        q: search
      }
    })

    this.setState({
      videos: response.data.items,
      selectedVideo: response.data.items[0]
    });
  }

  onVideoSelect = (video) => {
    this.setState({
      selectedVideo: video
    });
  }

  render() {
    return (
      <div className="App ui container">
        <Header 
          title="YouTube API with ReactJS"
        />
        <SearchBar 
          searchSubmit={this.searchSubmit}
        />
        <div className="ui grid stackable">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail 
                selectedVideo={this.state.selectedVideo}
              />
            </div>
            <div className="five wide column">
              <VideoList
                videos={this.state.videos}
                onVideoSelect={this.onVideoSelect}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
