import React from 'react';

const Header = (props) => {
    return ( 
        <header>
            <h1 className="ui header">{props.title}</h1>
        </header>
     );
}
 
export default Header;