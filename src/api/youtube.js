import axios from 'axios';

const key = 'AIzaSyDyr2rFnKOXcFbO4DKyS3ulKUh2gjo4RjA';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 25,
        key: key,
        type: 'video'
    }
});

